/*
 * Copyright (C) 2014 Francesco Azzola
 *  Surviving with Android (http://www.survivingwithandroid.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.test.weather2.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;


import com.survivingwithandroid.weather.lib.WeatherClient;
import com.survivingwithandroid.weather.lib.WeatherConfig;
import com.survivingwithandroid.weather.lib.exception.WeatherLibException;
import com.survivingwithandroid.weather.lib.model.WeatherForecast;
import com.test.weather2.R;
import com.test.weather2.adapter.WeatherAdapter;
import com.test.weather2.util.WeatherUtil;

public class ForecastWeatherFragment extends com.test.weather2.fragment.WeatherFragment {

    private SharedPreferences prefs;
    private ListView forecastList;
    private WeatherAdapter weatherAdapter;
    private WeatherConfig config;

    public static ForecastWeatherFragment newInstance() {
        ForecastWeatherFragment fragment = new ForecastWeatherFragment();
        return fragment;
    }

    public ForecastWeatherFragment() {
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.forecast_fragment, container, false);
        forecastList = (ListView) v.findViewById(R.id.forecastDays);
        return v;

    }

    public void refreshData() {
        refresh();
    }

    @Override
    public void onStart() {
        super.onStart();
        refresh();
    }

    private void refresh() {
        // Update forecast

        config = new WeatherConfig();

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        //  String cityId = prefs.getString("cityid", null);
        String cityId = prefs.getString("cityid", "1784658");
        Log.d("Swa", "City Id [" + cityId + "]");

        if (cityId == null) {
            getListener().requestCompleted();
            return ;
        }

        //  config.lang = WeatherUtil.getLanguage(prefs.getString("swa_lang", "en"));
        config.maxResult = 3;
        config.numDays = 3;

        String unit = prefs.getString("swa_temp_unit", "c");
        if (unit.equals("c"))
            config.unitSystem = WeatherConfig.UNIT_SYSTEM.M;
        else
            config.unitSystem = WeatherConfig.UNIT_SYSTEM.I;

        String lang = prefs.getString("swa_lang", "cn");
        if(lang.equals("cn"))
            config.lang = WeatherUtil.getLanguage(prefs.getString("swa_lang", "cn"));
        else
            config.lang = WeatherUtil.getLanguage(prefs.getString("swa_lang", "en"));

        weatherClient.updateWeatherConfig(config);

        weatherClient.getForecastWeather(cityId, new WeatherClient.ForecastWeatherEventListener() {
            @Override
            public void onWeatherRetrieved(WeatherForecast forecast) {
                WeatherAdapter adp = new WeatherAdapter(forecast, getActivity());
                forecastList.setAdapter(adp);
            }

            @Override
            public void onWeatherError(WeatherLibException t) {

            }

            @Override
            public void onConnectionError(Throwable t) {
                //WeatherDialog.createErrorDialog("Error parsing data. Please try again", MainActivity.this);
            }
        });
    }
}